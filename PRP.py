import pygame
from data.code.constants import SCREEN_SIZE
import data.code.scenarios.overworld.scene as OVERWORLD
from data.code.savemanager import SaveManager

class Game:
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode(SCREEN_SIZE)
		self.save = SaveManager()
		#SaveManager.Load_Save()
		self.clock = pygame.time.Clock() #Reloj

		self.overworld = OVERWORLD.Overworld(self)
		#self.run()

g = Game()
