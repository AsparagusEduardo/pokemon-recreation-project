import os

#Modifiable settings
SHOW_FPS = True

#Screen settings
MAX_FPS = 63
SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = 720, 432 #15x9
WINDOW_TITLE = 'Pokémon Recreation Project'

TILESIZE = 48

#Path settings
MAIN_DIR = os.path.dirname(os.path.dirname(os.path.split(os.path.abspath(__file__))[0]))
DATA_DIR = os.path.join(MAIN_DIR, 'data')

IMG_DIR = os.path.join(DATA_DIR, 'img')
IMG_TILESET_DIR = os.path.join(IMG_DIR, 'tilesets')

MAP_DIR = os.path.join(DATA_DIR, 'maps')

TILESET_DIR = os.path.join(DATA_DIR, 'tilesets')

SAVE_DIR = os.path.join(DATA_DIR, 'save')
SAVE_TMP_DIR = os.path.join(SAVE_DIR, 'tmp')

