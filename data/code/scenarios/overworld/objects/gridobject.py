import pygame
from .... import constants as CONSTANTS

class GridObject(pygame.sprite.Sprite):
	def __init__(self, overworld, x, y, collisions):

		self.current_xspeed = 0
		self.current_yspeed = 0
		self.moving = False
		self.movementBuffer = []

		screen = pygame.display.get_surface()

		self.area = screen.get_rect()

		self.grid_x = x
		self.grid_y = y
		self.collisions = collisions

