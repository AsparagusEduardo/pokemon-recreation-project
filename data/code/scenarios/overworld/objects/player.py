import os
import pygame
from ....functions import rendering as RENDER
from .gridobject import GridObject
from .... import constants as CONSTANTS

class Player(GridObject):
	def __init__(self, overworld, x, y):
		super().__init__(overworld, x, y, '1111')
		self.overworld = overworld
		
		self.groups = overworld.entity_sprites
		pygame.sprite.Sprite.__init__(self, self.groups)
		
		self.image, self.rect = RENDER.load_image('entities\overworld\player\player1.png',-1)
		self.rect.topleft = self.grid_x * CONSTANTS.TILESIZE, self.grid_y * CONSTANTS.TILESIZE
		
	
	def update(self):
		self._move()
	
	def check_GridCollision(self, direction):
		for wall in self.overworld.tilemap_collisions:
			if self.grid_y == wall.grid_y and self.grid_x == wall.grid_x:
				if direction == 'UP' and wall.collisions[0] == '1':
					return True
				elif direction == 'DOWN' and wall.collisions[1] == '1':
					return True
				elif direction == 'LEFT' and wall.collisions[2] == '1':
					return True
				elif direction == 'RIGHT' and wall.collisions[3] == '1':
					return True
					
			check_x = 0
			check_y = 0
			if direction == 'UP':
				check_y = -1
			elif direction == 'DOWN':
				check_y = 1
			elif direction == 'LEFT':
				check_x = -1
			elif direction == 'RIGHT':
				check_x = 1
			
			if self.grid_y + check_y == wall.grid_y and self.grid_x + check_x == wall.grid_x:
				if direction == 'UP' and wall.collisions[1] == '1':
					return True
				elif direction == 'DOWN'  and wall.collisions[0] == '1':
					return True
				elif direction == 'LEFT' and wall.collisions[3] == '1':
					return True
				elif direction == 'RIGHT' and wall.collisions[2] == '1':
					return True
		
		return False
		
	def _move(self):
		#print ('X:'+ str(self.rect.x) + ' Y:'+ str(self.rect.y) + ' Buffer:' +  str(self.movementBuffer))
		if self.rect.x % CONSTANTS.TILESIZE == 0 and self.rect.y % CONSTANTS.TILESIZE == 0:
			self.moving = False
			self.current_yspeed = 0
			self.current_xspeed = 0
			self.grid_x = self.rect.x / CONSTANTS.TILESIZE
			self.grid_y = self.rect.y / CONSTANTS.TILESIZE

		if self.moving == False and len(self.movementBuffer) > 0:
			if not self.check_GridCollision(self.movementBuffer[-1]):
				self.moving = True
				if self.movementBuffer[-1] == 'UP':
					self.current_yspeed = -2
				elif self.movementBuffer[-1] == 'DOWN':
					self.current_yspeed = 2
				elif self.movementBuffer[-1] == 'LEFT':
					self.current_xspeed = -2
				elif self.movementBuffer[-1] == 'RIGHT':
					self.current_xspeed = 2

		newpos = self.rect.move((self.current_xspeed, self.current_yspeed))
		self.rect = newpos
