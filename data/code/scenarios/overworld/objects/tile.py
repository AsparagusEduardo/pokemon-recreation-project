import os
import pygame
from ....functions import rendering as RENDER
from .gridobject import GridObject
from ....constants import TILESIZE

class Tile(GridObject):
	def __init__(self, overworld, x, y, layer, properties, image):
	
		collision_top = str(properties['collision_top'])
		collision_bottom = str(properties['collision_bottom'])
		collision_left = str(properties['collision_left'])
		collision_right = str(properties['collision_right'])
		
		collisions = collision_top + collision_bottom + collision_left + collision_right
		#print('L:' + str(layer) + ' C:' + collisions)
		super().__init__(overworld, x, y, collisions)
		
		if layer == 0:
			self.groups = overworld.tilemap_layer0, overworld.tilemap_collisions
		elif layer == 1:
			self.groups = overworld.tilemap_layer1, overworld.tilemap_collisions
		elif layer == 2:
			self.groups = overworld.tilemap_layer2, overworld.tilemap_collisions
		elif layer == 3:
			self.groups = overworld.tilemap_layer3, overworld.tilemap_collisions
		else:
			self.groups = overworld.tilemap_collisions
		
		#self.groups = overworld.tilemap_collisions
		
		self.image, self.rect = image, image.get_rect()
		pygame.sprite.Sprite.__init__(self, self.groups)
		
		self.rect.topleft = self.grid_x * TILESIZE, self.grid_y * TILESIZE
