import pygame
from pygame.locals import *
from ...constants import SCREEN_WIDTH, SCREEN_HEIGHT, TILESIZE, MAX_FPS, WINDOW_TITLE, SHOW_FPS
from .objects import player as PLAYER
from ..battle import scene as BATTLE
from .objects import tile as TILE
from .tilemap import Map, Camera
import threading
import time

class Overworld:
	def __init__(self, Game):
		self.Game = Game
		
		self.tilemap_layer0 = pygame.sprite.Group()
		self.tilemap_layer1 = pygame.sprite.Group()
		self.entity_sprites = pygame.sprite.Group()
		self.tilemap_layer2 = pygame.sprite.Group()
		self.tilemap_layer3 = pygame.sprite.Group()
		self.tilemap_collisions = pygame.sprite.Group()
		
		self.load_mapdata()
		self.load_elements()
		self.load_background()
		self.run()

	def load_mapdata(self):
		self.map = Map('001_PalletTown.tmx', self)
		#self.map2 = Map('002_Route1.tmx', self)
		pass

	def load_elements(self):
		grid_x = self.Game.save.player_pos_x
		grid_y = self.Game.save.player_pos_y
		
		self.player = PLAYER.Player(self, grid_x, grid_y)
		self.map.load_tiles()
		#self.map2.load_tiles()
		self.camera = Camera(self.map.width, self.map.height)
		#self.camera = Camera(SCREEN_WIDTH*20, SCREEN_HEIGHT*20)
		
					
	def load_background(self):
		#White background
		self.Game.screen.fill((250,250,250))

		#Grid lines
		for x in range(0, SCREEN_WIDTH, TILESIZE):
			pygame.draw.line(self.Game.screen, (0, 0, 0), (x, 0), (x, SCREEN_HEIGHT))
		for y in range(0, SCREEN_HEIGHT, TILESIZE):
			pygame.draw.line(self.Game.screen, (0, 100, 0), (0, y), (SCREEN_WIDTH, y))
			
	def update(self):
		self.tilemap_layer0.update()
		self.tilemap_layer1.update()
		self.entity_sprites.update()
		self.tilemap_layer2.update()
		self.tilemap_layer3.update()
		self.camera.update(self.player)
		fps = ""
		if SHOW_FPS:
			fps = ' - FPS:' + "{0:.1f}".format(self.Game.clock.get_fps())
		pygame.display.set_caption(WINDOW_TITLE + fps)
	
	def run(self):
		#Main Loop
		self.going = True
		
		#thread1 = MapRenderer(1, "Thread-1", self)
		#thread1.start()
		
		while self.going:
			self.Game.clock.tick(MAX_FPS)
			#STUFF

			for event in pygame.event.get():
				if event.type == QUIT:
					self.going = False
				elif event.type == KEYDOWN:
					if event.key == K_ESCAPE:
						self.going = False
					elif event.key == K_LEFT:
						self.entity_sprites.sprites()[0].movementBuffer.append('LEFT')
					elif event.key == K_RIGHT:
						self.entity_sprites.sprites()[0].movementBuffer.append('RIGHT')
					elif event.key == K_UP:
						self.entity_sprites.sprites()[0].movementBuffer.append('UP')
					elif event.key == K_DOWN:
						self.entity_sprites.sprites()[0].movementBuffer.append('DOWN')
					elif event.key == K_b:
						self.battle = BATTLE.Battle(self.Game)
				elif event.type == KEYUP:
					if event.key == K_LEFT:
						self.entity_sprites.sprites()[0].movementBuffer.remove('LEFT')
					if event.key == K_RIGHT:
						self.entity_sprites.sprites()[0].movementBuffer.remove('RIGHT')
					if event.key == K_UP:
						self.entity_sprites.sprites()[0].movementBuffer.remove('UP')
					if event.key == K_DOWN:
						self.entity_sprites.sprites()[0].movementBuffer.remove('DOWN')
			self.update()
			#Draw
			self.load_background()
			for sprite in self.tilemap_layer0:
				self.Game.screen.blit(sprite.image, self.camera.apply(sprite))
			for sprite in self.tilemap_layer1:
				self.Game.screen.blit(sprite.image, self.camera.apply(sprite))
			for sprite in self.entity_sprites:
				self.Game.screen.blit(sprite.image, self.camera.apply(sprite))
			for sprite in self.tilemap_layer2:
				self.Game.screen.blit(sprite.image, self.camera.apply(sprite))
			for sprite in self.tilemap_layer3:
				self.Game.screen.blit(sprite.image, self.camera.apply(sprite))
			self.tilemap_layer1.draw(self.Game.screen)
			self.tilemap_layer2.draw(self.Game.screen)
			self.entity_sprites.draw(self.Game.screen)
			self.tilemap_layer3.draw(self.Game.screen)
			pygame.display.flip()


		pygame.quit()

class MapRenderer(threading.Thread):
	def __init__(self, threadID, name, Overworld):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.overworld = Overworld
	def run(self):
		print ("Starting " + self.name)
		self.print_time()
		print ("Exiting " + self.name)
	  
	def print_time(self):
		contador = 0
		while self.overworld.going:
			time.sleep(1)
			print ("%s: %s" % (self.name, time.ctime(time.time())))
			print (self.overworld.tilemap_layer1)
			image = pygame.Surface((10,10)).convert_alpha()
			image.fill((40,40,40))
			self.overworld.tilemap_layer1.add(TILE.Tile(self.overworld, self.overworld.player.grid_x, self.overworld.player.grid_y, 0, {'collision_top': 0, 'collision_bottom': 0, 'collision_left': 0,'collision_right': 0}, image))
			contador +=1
			