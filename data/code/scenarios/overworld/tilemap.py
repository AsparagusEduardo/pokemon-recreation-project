from os import path
import pygame as py
from pytmx.util_pygame import load_pygame
from ...constants import TILESIZE, MAP_DIR, SCREEN_WIDTH, SCREEN_HEIGHT, TILESET_DIR
from .objects import tile as TILE
from ...functions.rendering import load_tile

class Map:
	def __init__(self, filename, overworld):
		self.overworld = overworld
		
		self.tiled_map = load_pygame(path.join(MAP_DIR, filename))
		
		self.width = self.tiled_map.width
		self.height = self.tiled_map.height
		
	def load_tiles(self):
		readingLayer = 0
		for layer in self.tiled_map.layers:
			for x, y, image in layer.tiles():
				if self.tiled_map.get_tile_properties(x, y, readingLayer) != None:
					TILE.Tile(self.overworld, x, y, readingLayer, self.tiled_map.get_tile_properties(x, y, readingLayer), image)
			readingLayer += 1
				
class Camera:
	def __init__(self, width, height):
		self.camera = py.Rect(0, 0, width, height)
		self.width = width
		self.height = height
		
	def apply(self, entity):
		return entity.rect.move(self.camera.topleft)
		
	def update(self, target):
		x = -target.rect.x + int(SCREEN_WIDTH/2 - TILESIZE/2)
		y = -target.rect.y + int(SCREEN_HEIGHT/2 - TILESIZE/2)
		
		'''
		#Limit scrolling to map size
		x = min(0, x) #Left
		y = min(0, y) #Up
		x = max(-(self.width - SCREEN_WIDTH), x)
		y = max(-(self.height - SCREEN_HEIGHT), y)
		'''
		
		self.camera = py.Rect(x, y, self.width, self.height)