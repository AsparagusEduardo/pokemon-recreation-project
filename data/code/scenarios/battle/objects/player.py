import os
import pygame
from ....functions import rendering as RENDER
from .gridobject import GridObject
from .... import constants as CONSTANTS

class Player(GridObject):
	def __init__(self, x, y):
		super().__init__(x, y)
		self.image, self.rect = RENDER.load_image('entities\overworld\player\player1.png',-1)
		self.rect.topleft = self.grid_x * CONSTANTS.TILESIZE, self.grid_y * CONSTANTS.TILESIZE
