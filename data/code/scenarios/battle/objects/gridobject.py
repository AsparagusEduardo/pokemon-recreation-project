import pygame
from .... import constants as CONSTANTS

class GridObject(pygame.sprite.DirtySprite):
	def __init__(self, x, y):
		pygame.sprite.DirtySprite.__init__(self)

		self.current_xspeed = 0
		self.current_yspeed = 0
		self.moving = False
		self.movementBuffer = []

		screen = pygame.display.get_surface()

		self.area = screen.get_rect()

		self.grid_x = x
		self.grid_y = y

	def update(self):
		self._move()
		self.dirty = 1

	def _move(self):
		#print ('X:'+ str(self.rect.x) + ' Y:'+ str(self.rect.y) + ' Buffer:' +  str(self.movementBuffer))
		if self.rect.x % CONSTANTS.TILESIZE == 0 and self.rect.y % CONSTANTS.TILESIZE == 0:
			self.moving = False
			self.current_yspeed = 0
			self.current_xspeed = 0

		if self.moving == False and len(self.movementBuffer) > 0:
			self.moving = True
			if self.movementBuffer[-1] == 'UP':
				self.current_yspeed = -2
			elif self.movementBuffer[-1] == 'DOWN':
				self.current_yspeed = 2
			elif self.movementBuffer[-1] == 'LEFT':
				self.current_xspeed = -2
			elif self.movementBuffer[-1] == 'RIGHT':
				self.current_xspeed = 2

		newpos = self.rect.move((self.current_xspeed, self.current_yspeed))
		self.rect = newpos
