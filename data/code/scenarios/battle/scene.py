import pygame
from pygame.locals import *
from ... import constants as CONSTANTS
from .objects import player as PLAYER

class Battle:
    def __init__(self, Game):
        self.Game = Game
        self.load_elements()
        self.run()
    def load_elements(self):

        #White background
        background = pygame.Surface(self.Game.screen.get_size())
        background = background.convert()
        background.fill((0,250,100))

        #Grid lines
        for x in range(0, CONSTANTS.SCREEN_WIDTH, CONSTANTS.TILESIZE):
            pygame.draw.line(background, (0, 0, 0), (x, 0), (x, CONSTANTS.SCREEN_HEIGHT))
        for y in range(0, CONSTANTS.SCREEN_HEIGHT, CONSTANTS.TILESIZE):
            pygame.draw.line(background, (0, 100, 0), (0, y), (CONSTANTS.SCREEN_WIDTH, y))

        # Game Objects
        player = PLAYER.Player(5,5)
        # Sprite groups
        self.Game.playersprites = pygame.sprite.LayeredDirty((player))
        #Background
        self.Game.playersprites.clear(self.Game.screen, background)
    def run(self):


        #Main Loop
        going = True
        while going:
            self.Game.clock.tick(CONSTANTS.MAX_FPS)
            #STUFF

            for event in pygame.event.get():
                if event.type == QUIT:
                    going = False
                elif event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        going = False
                    elif event.key == K_LEFT:
                        self.Game.playersprites.sprites()[0].movementBuffer.append('LEFT')
                        pass
                        #player.grid_move('LEFT')
                    elif event.key == K_RIGHT:
                        self.Game.playersprites.sprites()[0].movementBuffer.append('RIGHT')
                        #player.grid_move('RIGHT')
                        pass
                    elif event.key == K_UP:
                        self.Game.playersprites.sprites()[0].movementBuffer.append('UP')
                        #player.grid_move('UP')
                        pass
                    elif event.key == K_DOWN:
                        self.Game.playersprites.sprites()[0].movementBuffer.append('DOWN')
                        #player.grid_move('DOWN')
                        pass
                elif event.type == KEYUP:
                    if event.key == K_LEFT:
                        self.Game.playersprites.sprites()[0].movementBuffer.remove('LEFT')
                        pass
                    if event.key == K_RIGHT:
                        self.Game.playersprites.sprites()[0].movementBuffer.remove('RIGHT')
                        pass
                    if event.key == K_UP:
                        self.Game.playersprites.sprites()[0].movementBuffer.remove('UP')
                        pass
                    if event.key == K_DOWN:
                        self.Game.playersprites.sprites()[0].movementBuffer.remove('DOWN')
                        pass
            self.Game.playersprites.update()
            #Draw
            rects = self.Game.playersprites.draw(self.Game.screen)
            pygame.display.update(rects)
