from os import path
import shutil
from ..code.constants import SAVE_DIR, SAVE_TMP_DIR

class SaveManager:
	def __init__(self):
		self.player_pos_x = 0
		self.player_pos_y = 0
		self.sav_to_tmp()
		self.load_tmp()
	def load_tmp(self):
		with open(path.join(SAVE_TMP_DIR, 'player.sav'), 'rt') as f:
			for row, line in enumerate(f):
				attribute = line.strip().split(':')[0]
				value = line.strip().split(':')[1]
				
				if attribute == 'x_pos':
					self.player_pos_x = int(value)
				elif attribute == 'y_pos':
					self.player_pos_y = int(value) + 1
		pass
		
	def sav_to_tmp(self):
		shutil.copy(path.join(SAVE_DIR, 'player.sav'), SAVE_TMP_DIR)
		pass