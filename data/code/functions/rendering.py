import os
import pygame
from pygame.locals import *
from pygame.compat import geterror
from ..constants import IMG_DIR, TILESIZE, IMG_TILESET_DIR

def load_image(name, colorkey=None):
	fullname = os.path.join(IMG_DIR, name)
	try:
		image = pygame.image.load(fullname)
		#print ('Loaded image: ' + name)
	except pygame.error:
		print ('Cannot load image:', fullname)
		raise SystemExit(str(geterror()))
	image = image.convert_alpha()
	if colorkey is not None:
		if colorkey is -1:
			colorkey = image.get_at((0, 0))
		image.set_colorkey(colorkey, RLEACCEL)
	return image, image.get_rect()
	
def load_tile(tileset_id, x, y, colorkey=None):
	if tileset_id == 1:
		name = 'FRLG_Terrain_2.png'
	else:
		name = 'FRLG_Terrain.png'
	fullname = os.path.join(IMG_TILESET_DIR, name)
	#print (str(x) + ':' + str(y))
	try:
		image = pygame.Surface([TILESIZE, TILESIZE], pygame.SRCALPHA).convert_alpha()
		image.blit(pygame.image.load(fullname).convert_alpha(), (-1 * TILESIZE * x, -1 * TILESIZE * y))
	except pygame.error:
		print ('Cannot load image:', fullname)
		raise SystemExit(str(geterror()))
	image = image.convert_alpha()
	if colorkey is not None:
		if colorkey is -1:
			colorkey = image.get_at((0, 0))
		image.set_colorkey(colorkey, RLEACCEL)
	return image
	
